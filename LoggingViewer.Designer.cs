﻿namespace Logging.Ui
{
    partial class LoggingViewer
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // LoggingViewer
            //
            this.Name = "LoggingViewer";
            this.ResumeLayout(false);
            this.Multiline = true;
            this.ReadOnly = true;
            this.WordWrap = false;
            this.Font = new System.Drawing.Font(System.Drawing.FontFamily.GenericMonospace.Name, Font.Size);
        }

        #endregion
    }
}
