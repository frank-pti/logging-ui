﻿using Gtk;

namespace Logging.Ui
{
    /// <summary>
    /// Text view for displaying the log text.
    /// </summary>
    public class LoggingView : TextView
    {
        /// <summary>
        /// Create new logging text view.
        /// </summary>
        /// <param name="textBuffer">The text buffer to display.</param>
        public LoggingView(TextBuffer textBuffer) :
            this(textBuffer, "Monospace", 12)
        {
        }

        /// <summary>
        /// Create new logging text view.
        /// </summary>
        /// <param name="textBuffer">The text buffer to display.</param>
        /// <param name="fontFamily">The font family to use.</param>
        /// <param name="size">The font size to use.</param>
        public LoggingView(TextBuffer textBuffer, string fontFamily, int size) :
            base(textBuffer)
        {
            Editable = false;
            Pango.FontDescription font = new Pango.FontDescription() {
                Family = fontFamily,
                AbsoluteSize = size * Pango.Scale.PangoScale
            };
            ModifyFont(font);
        }
    }
}
