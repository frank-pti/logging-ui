﻿using Gtk;
using System;

namespace Logging.Backends
{
    /// <summary>
    /// Backend for logging into a GTK# TextBuffer (to be used e.g. in a TextView)
    /// </summary>
    public class TextBufferBackend : Backend
    {
        /// <summary>
        /// Create new backend for specified text buffer.
        /// </summary>
        /// <param name="buffer"></param>
        public TextBufferBackend(TextBuffer buffer)
        {
            TextBuffer = buffer;
        }

        /// <inheritdoc/>
        public override void Log(DateTime timeStamp, LogLevel level, string message)
        {
            if (Enabled) {
                TextIter endIter = TextBuffer.EndIter;
                TextBuffer.Insert(ref endIter, BuildLogLine(timeStamp, level, message));
                TextBuffer.Insert(ref endIter, Environment.NewLine);
            }
        }

        /// <summary>
        /// Gets the text buffer.
        /// </summary>
        public TextBuffer TextBuffer { get; private set; }
    }
}
