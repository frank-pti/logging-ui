﻿using System;
using System.Windows.Forms;

namespace Logging.Backends
{
    /// <summary>
    /// Backend for logging into a Windows Formas TextBox
    /// </summary>
    public class TextBoxBackend : Backend
    {
        private TextBox textBox;

        /// <summary>
        /// Create backend for the specified textbox.
        /// </summary>
        /// <param name="textBox"></param>
        public TextBoxBackend(TextBox textBox)
        {
            this.textBox = textBox;
        }

        /// <inheritdoc/>
        public override void Log(DateTime timeStamp, LogLevel level, string message)
        {
            textBox.AppendText(BuildLogLine(timeStamp, level, message));
            textBox.AppendText(Environment.NewLine);
        }
    }
}
